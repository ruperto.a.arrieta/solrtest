<?php

namespace App\Http\Controllers;//ORIGINAL

use Illuminate\Http\Request; //ORIGINAL
//use Solarium\Client; //ADDED 181015

class SolariumController extends Controller //ORIGINAL
{
    protected $client;

    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;
    }

    public function ping()
    {
        // create a ping query
        $ping = $this->client->createPing();

        // execute the ping query
        try {
            $this->client->ping($ping);
            return response()->json('OK');
        } catch (\Solarium\Exception $e) {
            return response()->json('ERROR', 500);
        }
    }

    public function search()
    {
		$client= $this->client;
        $query = $client->createSelect();
        //$query->addFilterQuery(array('key'=>'provence', 'query'=>'provence:Groningen', 'tag'=>'include'));
        //$query->addFilterQuery(array('key'=>'degree', 'query'=>'degree:MBO', 'tag'=>'exclude'));
        //$facets = $query->getFacetSet();
        //$facets->createFacetField(array('field'=>'degree', 'exclude'=>'exclude'));
        $resultset = $client->select($query);

        // display the total number of documents found by solr
        echo 'NumFound: ' . $resultset->getNumFound();

        // show documents using the resultset iterator
        foreach ($resultset as $document) {

            echo '<hr/><table>';

            // the documents are also iterable, to get all fields
            foreach ($document as $field => $value) {
                // this converts multivalue fields to a comma-separated string
                if (is_array($value)) {
                    $value = implode(', ', $value);
                }

                echo '<tr><th>' . $field . '</th><td>' . $value . '</td></tr>';
            }

            echo '</table>';
        }
    }
	//START ADDED 181012 230pm
	public function find(){
		$client= $this->client;
        $query = $client->createSelect();
		$resultset = $client->select($query);
		echo 'NumFound: ' . $resultset->getNumFound().'<br>';  // display the total number of documents found by solr
		echo print_r($resultset, true);
		
	}
	
	public function find2(){
		//use Solarium\Client;
		$client = new \Solarium\Client($config);// new Solarium\Client($config);
		$query = $client->createSelect();

		//$facetSet = $query->getFacetSet();
		//$facetSet->createFacetField('stock')->setField('inStock');

		$resultset = $client->select($query);
		echo 'NumFound: '.$resultset->getNumFound() . PHP_EOL;

		//$facet = $resultset->getFacetSet()->getFacet('stock');
		//foreach ($facet as $value => $count) {
		//	echo $value . ' [' . $count . ']' . PHP_EOL;
		//}

		foreach ($resultset as $document) {
			echo $document->id . PHP_EOL;
			echo $document->name . PHP_EOL;
		}
	}
}