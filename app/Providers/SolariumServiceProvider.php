<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Solarium\Client;//ADDED  181010
class SolariumServiceProvider extends ServiceProvider
{
	protected $defer = true; //ADDED  181010
	/**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
		$this->app->bind(Client::class, function ($app) { //ADDED  181010
            return new Client($app['config']['solarium']); //ADDED  181010
        });  //ADDED  181010
    }
	
	public function provides()
    {
        return [Client::class];
    }
}
