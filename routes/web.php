<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ping', 'SolariumController@ping'); //ADDED 181010

Route::get('/search', 'SolariumController@search'); //ADDED 181010

Route::get('/find', 'SolariumController@find'); //ADDED 181015

Route::get('/find2', 'SolariumController@find2'); //ADDED 181015